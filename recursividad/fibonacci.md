# Ejemplo de recursividad - Serie de Fibonacci

## Clase donde implementamos la serie Fibonacci de forma iterativa y recursiva

```java
package fibonacci;


public class fibonacci {

    private long cont = 0;

    //recursivo 
    public long calcularFibonacciR (long n){
        System.out.println( ++cont );
        if (n>1)
    return calcularFibonacciR(n-1)+ calcularFibonacciR(n-2);
        else
    if(n==0)
        return 0;
     else
    if(n==1)
        return 1;
   
    return calcularFibonacciR(n-1)+ calcularFibonacciR(n-2);

    }

    //iterativo 
    public long calcularFibonacciI (long n){
        if(n==0)
        return 0;
        long fn=1,f1=1,f2=0,i;
        for(i=1;i<n;i++){
            fn=f1+f2;
            f2=f1;
            f1=fn;
        }

        return fn;
    }
}
```

## Clase Main

```java
 */
package fibonacci;

import java.util.Scanner;

public class FibonacciMain {

   
    public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa Un Numero Para Calcular La Serie De Fibonacci:\n");
        long num = sc.nextLong();
        
        fibonacci fb = new fibonacci();
        System.out.println("[ITERATIVO] Fibonacci creado:  "+ fb.calcularFibonacciI(num)+"\n");
        System.out.println("[RECURSIVO] Fibonacci creado:  "+ fb.calcularFibonacciR(num)+"\n");
         
    }
    
}
```